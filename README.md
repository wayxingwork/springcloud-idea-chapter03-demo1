# springcloud-idea-chapter03-a  Ribbon客户端负载均衡

小组成员：（2-4人）

学号+姓名

角色

一、实验目的
高可用架构实验（单注册中心）

二、实验仪器设备与环境

1、Eureka Server（1个）

2、Provider （2个）

3、Consumer （1个）

三、实验原理

Eureka Server + Provider + Consumer（Ribbon）

![](./doc/x.png)

Server保存注册信息

Provider提供基本服务响应，返回JSON数据

Consumer利用负载均衡算法调用Provider服务

四、实验内容与步骤

配置Maven环境

新建项目

创建码云项目

小组克隆项目

根据角色修改配置文件，调试Provider和ribbon-client

五、实验结果与分析

分析测试当Provider1或者Provider2停机后，访问Consumer会有什么效果？

Eureka server停机后，访问Consumer会出现什么现象？

六、结论与体会

1）体会 Eureka Server、Provider和Consumer角色各自的作用。

2）说明 Ribbon在哪个角色中扮演什么功能。